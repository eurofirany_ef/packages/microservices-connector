<?php

namespace Eurofirany\Microservices\Providers;

use Illuminate\Support\ServiceProvider;

class MicroservicesConnectorServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/http.php', 'http_config');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/http.php' => app()->configPath('http.php'),
        ], 'http_config');
    }
}
