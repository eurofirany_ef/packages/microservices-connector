<?php

namespace Eurofirany\Microservices\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property PricesContractorResponse result
 * Class PricesContractorResponse
 * @package Eurofirany\Microservices\Responses
 */
class PricesContractorsResponse extends CanCast {
    protected array $map = ['result' => PricesContractorsResponse::class];
}