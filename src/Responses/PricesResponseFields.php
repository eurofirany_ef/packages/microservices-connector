<?php

namespace Eurofirany\Microservices\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property float CENA_S
 * @property float CENA_F
 * @property float CENA
 * @property float CENA_D
 * @property float CENA_P
 * @property float CENA_11
 * @property float CENA_12
 * @property float CENA_13
 * @property float CENA_14
 * @property float CENA_15
 * @property float CENA_Z
 * @property float suggested
 * @property float fixed
 * Class PricesResponseFields
 * @package Eurofirany\Microservices\Responses
 */
class PricesResponseFields extends CanCast {}