<?php

namespace Eurofirany\Microservices\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property int id
 * @property string code
 * @property bool active
 * @property int batch
 * @property string|null created_at
 * @property string|null updated_at
 * Class PricesContractorResponse
 * @package Eurofirany\Microservices\Responses
 */
class PricesContractorResponse extends CanCast {}