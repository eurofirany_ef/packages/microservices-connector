<?php

namespace Eurofirany\Microservices\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property string|int sku
 * @property int tax_rate
 * @property float price
 * Class PricesByFieldElementResponse
 * @package Eurofirany\Microservices\Responses
 */
class PricesByFieldElementResponse extends CanCast {}