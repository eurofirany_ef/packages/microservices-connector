<?php

namespace Eurofirany\Microservices\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property string sku
 * @property int quantity
 * Class ProductStockElement
 */
class StockElementResponse extends CanCast {}