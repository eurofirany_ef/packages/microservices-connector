<?php

namespace Eurofirany\Microservices\Responses;

use Eurofirany\CastToClass\CanCast;

/**
 * @property string status
 * Class TriggerResponse
 * @package Eurofirany\Microservices\Responses
 */
class StockTriggerResponse extends CanCast
{}