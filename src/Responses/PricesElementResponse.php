<?php

namespace Eurofirany\Microservices\Responses;

/**
 * @property string|int sku
 * @property boolean sale
 * @property PricesResponseFields basic
 * Class PricesElementResponse
 * @package Eurofirany\Microservices\Responses
 */
class PricesElementResponse extends PricesResponseFields {
    protected array $map = ['basic' => PricesResponseFields::class];
}