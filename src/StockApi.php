<?php

namespace Eurofirany\Microservices;

use Eurofirany\Microservices\Responses\StockElementResponse;
use Eurofirany\Microservices\Responses\StockTriggerResponse;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class StockApi extends HttpApi
{
    /**
     * Initialize connector
     */
    public function __construct()
    {
        $this->apiUrl = config('http.stock.url');
        $this->client = config('http.stock.client');
        $this->token = config('http.stock.token');

        parent::__construct();
    }

    /**
     * Get stock for product
     * @param string $productSku SKU of product for which stock will be taken
     * @return Collection|StockElementResponse[]
     * @throws GuzzleException
     */
    public function getProductStock(string $productSku): Collection|array
    {
        return $this->getProductsStock([$productSku]);
    }

    /**
     * Get products stock
     * @param array $productsSku Array of products sku for which stock will be taken
     * @param array $types Types for which stock will be taken
     * @param array $channels Channels for which stock will be taken
     * @return Collection|StockElementResponse[]
     * @throws GuzzleException
     */
    public function getProductsStock(
        array $productsSku,
        array $types = [],
        array $channels = []
    ): Collection|array
    {
        $stock = [];

        /** @var Collection $productsSkuCollection */
        foreach(collect($productsSku)->chunk(100) as $productsSkuCollection){
            $this->setEndPoint("list/sku");
            $this->addGetParameter('sku', $productsSkuCollection->toArray());
            $this->addGetParameter('types', $types);
            $this->addGetParameter('channels', $channels);

            $stock = array_merge($stock, $this->get()->json());
        }

        return StockElementResponse::castFromArrayOfItems($stock);
    }

    /**
     * Trigger stock update
     * @param array $skuArray Array of sku for which we want to trigger update of stock
     * @return StockTriggerResponse
     * @throws GuzzleException
     */
    public function triggerUpdate(array $skuArray): StockTriggerResponse
    {
        $this->setEndPoint('trigger');
        $this->addGetParameter('sku', $skuArray);

        return StockTriggerResponse::cast($this->get()->json());
    }
}
