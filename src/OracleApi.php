<?php

namespace Eurofirany\Microservices;

use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class OracleApi extends HttpApi
{
    /**
     * Init connector
     */
    public function __construct()
    {
        $this->apiUrl = config('http.oracle.url');
        $this->client = config('http.oracle.client');
        $this->token = config('http.oracle.token');

        parent::__construct();
    }

    /**
     * Get products from db by type
     * @param string $type type of products
     * @return Collection
     * @throws GuzzleException
     */
    public function getProductsForAllegro(string $type): Collection
    {
        $page = 1;

        $products = [];

        do {
            $this->addGetParameter('page', $page);
            $this->setEndPoint(
                sprintf("products/allegro/%s", $type)
            );

            $response = $this->get()->json();
            $products = array_merge($products, $response->data);

            $maxPage = $response->last_page;
            $page++;
        } while($page <= $maxPage);

        return collect($products);
    }

    /**
     * Get beams from database
     * @param string $tableName Table name for beams
     * @return Collection
     * @throws GuzzleException
     */
    public function getBeams(string $tableName): Collection
    {
        $this->setEndPoint(sprintf("stock/beams/%s", $tableName));

        return collect($this->get()->json());
    }

    /**
     * Get stock from database by table and channels
     * @param string $tableName Table name for stocks
     * @param array $channels Channels for stock
     * @param int $page Current request page
     * @return Collection
     * @throws GuzzleException
     */
    public function getByTableAndSalesChannels(
        string $tableName,
        array $channels = [],
        int $page = 1
    ): Collection
    {
        $this->setEndPoint(sprintf("stock/%s", $tableName));
        $this->addGetParameter('channels', $channels);
        $this->addGetParameter('page', $page);

        return collect($this->get()->json());
    }

    /**
     * Get stocks from database for sku
     * @param array|null $skuArray Array of sku for which stocks will be taken
     * @return Collection
     * @throws GuzzleException
     */
    public function getStocks(?array $skuArray): Collection
    {
        $this->setEndPoint('stock');

        if($skuArray)
            $this->addGetParameter('sku', $skuArray);

        return collect($this->get()->json());
    }

    /**
     * Get data of product
     * @param int $sku Product sku for which data will be taken
     * @return object|null
     * @throws GuzzleException
     */
    public function getProductData(int $sku): ?object
    {
        return $this->searchProducts(collect([$sku]))->first();
    }

    /**
     * Find products by sku
     * @param Collection $skuCollection Collection of sku numbers for products what we are looking for
     * @param string $type Type of products to get
     * @return Collection
     * @throws GuzzleException
     */
    public function searchProducts(Collection $skuCollection, string $type = 'eurofirany'): Collection
    {
        $productAdditionalData = [];

        /** @var Collection $skuArray */
        foreach($skuCollection->chunk(1000) as $skuArray) {
            $this->setEndPoint('products/search');
            $this->addBodyParameter('sku', $skuArray->toArray());
            $this->addBodyParameter('type', $type);

            $productAdditionalData = array_merge($productAdditionalData, $this->post()->json());
        }

        return collect($productAdditionalData);
    }

    /**
     * Get products from database
     * @param string $type Type of products to get
     * @param array $fields Field to get if empty get all
     * @return Collection
     * @throws GuzzleException
     */
    public function getProducts(string $type = 'eurofirany', array $fields = []): Collection
    {
        $productsData = [];
        $page = 1;

        do {
            $products = $this->getProductsPerPage($page, $type, $fields);
            $productsData = array_merge($productsData, $products);

            $page++;
        } while(count($products) > 0);

        return collect($productsData);
    }

    /**
     * Get product per page
     * @param int $page Current request page
     * @param string $type Type of products to get
     * @param array $fields Field of products to get if empty get all
     * @return array
     * @throws GuzzleException
     */
    public function getProductsPerPage(
        int $page = 1,
        string $type = 'eurofirany',
        array $fields = []
    ): array
    {
        $this->setEndPoint('products');
        $this->addGetParameter('type', $type);
        $this->addGetParameter('page', $page);
        $this->addGetParameter('fields', $fields);

        return $this->get()->json()->data;
    }
}
