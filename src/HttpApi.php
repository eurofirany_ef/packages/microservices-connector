<?php

namespace Eurofirany\Microservices;

use Eurofirany\ConnectorsQueue\ConnectorsQueue;
use Exception;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use JetBrains\PhpStorm\Pure;
use Psr\Http\Message\ResponseInterface;

abstract class HttpApi
{
    protected string $client;

    protected string $token;

    protected string $apiUrl;

    private string $endPoint = '';

    private string $getParameters = '';

    protected Client $httpClient;

    private array $body;

    private ConnectorsQueue $connectorsQueue;

    protected ResponseInterface $response;

    public function __construct()
    {
        $this->httpClient = new Client;

        $this->connectorsQueue = new ConnectorsQueue();

        $this->connectorsQueue->setting(
            config('http.queue.enabled', false),
            config('http.queue.max_tries', 3),
            config('http.queue.max_running_jobs', 5),
            config('http.queue.max_wait_seconds', 300),
            config('http.queue.sleep_seconds', 10),
            config('http.queue.max_per_minute', 0)
        );
    }

    /**
     * @return string
     */
    public function getGetParameters(): string
    {
        return $this->getParameters;
    }

    /**
     * @param string $getParameters
     */
    public function setGetParameters(string $getParameters): void
    {
        $this->getParameters = $getParameters;
    }

    /**
     * Set if you want to ignore queue for next requests or not
     * @param bool $ignoreRequestQueue
     */
    public function setIgnoreRequestQueue(bool $ignoreRequestQueue)
    {
        $this->connectorsQueue->setWithQueue(!$ignoreRequestQueue);
    }

    /**
     * @param string $endPoint
     */
    protected function setEndPoint(string $endPoint): void
    {
        $this->endPoint = $endPoint;
    }

    /**
     * @param string $apiUrl
     */
    protected function setApiUrl(string $apiUrl): void
    {
        $this->apiUrl = $apiUrl;
    }

    /**
     * @return HttpApi
     * @throws GuzzleException
     * @throws Exception
     */
    protected function get(): HttpApi
    {
        return $this->connectorsQueue->call(function () {
            $this->response = $this->httpClient->get($this->getRequestUrl());
            $this->setEndPoint('');
            $this->setGetParameters('');

            return $this;
        });
    }

    /**
     * @return HttpApi
     * @throws GuzzleException
     * @throws Exception
     */
    protected function post(): HttpApi
    {
        return $this->connectorsQueue->call(function () {
            $this->response = $this->httpClient->post($this->getRequestUrl(), [
                'form_params' => $this->body
            ]);

            $this->setEndPoint('');
            $this->setGetParameters('');

            return $this;
        });
    }

    /**
     * @param bool $associative
     * @return object|array
     */
    protected function json(bool $associative = false): object|array
    {
        return json_decode($this->text(), $associative);
    }

    /**
     * @return string
     */
    protected function text(): string
    {
        return $this->response->getBody()->getContents();
    }

    /**
     * @return string
     */
    #[Pure] protected function getRequestUrl(): string
    {
        return $this->apiUrl . $this->endPoint . sprintf(
                "?client=%s&token=%s",
                $this->client,
                $this->token
            ) . $this->getGetParameters();
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    protected function addBodyParameter(string $name, mixed $value): void
    {
        $this->body[$name] = $value;
    }

    /**
     * @param string $name
     * @param mixed $value
     */
    protected function addGetParameter(string $name, mixed $value): void
    {
        if (is_array($value))
            foreach ($value as $valueItem)
                $this->getParameters .= sprintf("&%s[]=%s", $name, $valueItem);
        else
            $this->getParameters .= sprintf("&%s=%s", $name, $value);
    }

    /**
     * @param string $method
     * @param string $type
     * @return HttpApi
     * @throws GuzzleException
     */
    public function request(
        string $method = 'POST',
        string $type = 'json'
    ): HttpApi
    {
        return $this->connectorsQueue->call(function () use ($method, $type) {
            $this->response = $this->httpClient->request($method, $this->getRequestUrl(), [
                $type => $this->body
            ]);

            $this->setEndPoint('');
            $this->setGetParameters('');

            return $this;
        });
    }

    /**
     * @param array $value
     */
    protected function setBody(array $value): void
    {
        $this->body = $value;
    }

    /**
     * @param array $options
     * @return HttpApi
     * @throws GuzzleException
     */
    public function requestFile(array $options): HttpApi
    {
        $this->response = $this->httpClient->request('POST', $this->getRequestUrl(),
            $options
        );

        $this->setEndPoint('');
        $this->setGetParameters('');

        return $this;
    }
}
