<?php

namespace Eurofirany\Microservices;

use Eurofirany\Microservices\Responses\PricesByFieldElementResponse;
use Eurofirany\Microservices\Responses\PricesContractorsResponse;
use Eurofirany\Microservices\Responses\PricesElementResponse;
use GuzzleHttp\Exception\GuzzleException;
use Illuminate\Support\Collection;

class PricesApi extends HttpApi
{
    /**
     * Init connector
     */
    public function __construct()
    {
        $this->apiUrl = config('http.prices.url');
        $this->client = config('http.prices.client');
        $this->token = config('http.prices.token');

        parent::__construct();
    }

    /**
     * Get prices for products
     * @param array $productsSku Sku of products for which we need to get prices
     * @param string $contractorCode Contractor code for which we will get prices
     * @param string $currency Currency for which we will get prices
     * @param array $discounts Additional discounts for prices
     * @return Collection|PricesElementResponse[]
     * @throws GuzzleException
     */
    public function getProductsPrices(
        array $productsSku,
        string $contractorCode,
        string $currency = "PLN",
        array $discounts = []
    ): Collection|array
    {
        $this->setEndPoint('prices');
        $this->addBodyParameter('sku', $productsSku);
        $this->addBodyParameter('contractor', $contractorCode);
        $this->addBodyParameter('currency', $currency);
        $this->addBodyParameter('discounts', $discounts);

        return PricesElementResponse::castFromArrayOfItems(
            $this->post()->json(true)
        );
    }

    /**
     * Get list of prices
     * @param string $contractorCode Code of contractor for which prices will be taken
     * @param string $currency Currency for prices
     * @param array $discounts Additional discounts
     * @param int $page Current request page number
     * @return PricesElementResponse[]|Collection
     * @throws GuzzleException
     */
    public function getPricesList(
        string $contractorCode,
        string $currency = "PLN",
        array $discounts = [],
        int $page = 1
    ): Collection|array
    {
        $this->setEndPoint('prices/list');
        $this->addGetParameter('page', $page);
        $this->addGetParameter('contractor', $contractorCode);
        $this->addGetParameter('currency', $currency);
        $this->addGetParameter('discounts', $discounts);

        return PricesElementResponse::castFromArrayOfItems($this->get()->json()->data);
    }


    /**
     * Get products prices only for one field
     * @param array $skuArray SKu of products for which prices will be taken
     * @param string $type Type of prices to get
     * @param string $field Field for which price will be taken
     * @return Collection|PricesByFieldElementResponse[]
     * @throws GuzzleException
     */
    public function getProductsPricesForField(
        array $skuArray,
        string $type,
        string $field
    ): Collection|array
    {
        /** @var Collection $skuCollection */
        foreach (collect($skuArray)->chunk(250) as $skuCollection) {
            $this->setEndPoint('prices/byField');
            $this->addBodyParameter('sku', $skuCollection->filter(function ($sku) {
                $sku = intval($sku);

                return $sku != 0;
            })->toArray());
            $this->addBodyParameter('type', $type);
            $this->addBodyParameter('field', $field);
            $this->addGetParameter('cache', 0);

            $prices = array_merge($prices ?? [], (array) $this->post()->json());
        }

        return PricesByFieldElementResponse::castFromArrayOfItems($prices ?? []);
    }

    /**
     * Get only these prices that changes
     * @param string $contractorCode Code of contractor for which prices will be taken
     * @param string $currency Currency for prices
     * @param array $discounts Additional discounts
     * @param array $fields Fields for which prices will be taken
     * @param int $daysToSub Days to sub, for example 1 will return prices that changes from yesterday
     * @return Collection|PricesElementResponse[]
     * @throws GuzzleException
     */
    public function getChangedProductsPrices(
        string $contractorCode,
        string $currency = "PLN",
        array $discounts = [],
        array $fields = [],
        int $daysToSub = 1,
    ): Collection|array
    {
        $page = 1;

        do {
            $this->setEndPoint('prices/changed');
            $this->addGetParameter('contractor', $contractorCode);
            $this->addGetParameter('currency', $currency);
            $this->addGetParameter('discounts', $discounts);
            $this->addGetParameter('fields', $fields);
            $this->addGetParameter('$daysToSub', $daysToSub);

            $response = $this->get()->json()->data;

            $prices = array_merge($prices ?? [], $response);

            $page++;
        } while (count($response) > 0);

        return PricesElementResponse::castFromArrayOfItems($prices ?? []);
    }

    /**
     * Store or update contractor data on prices
     * @param string $code Contractor code
     * @param bool $active Set contractor active or not
     * @return PricesContractorsResponse
     * @throws GuzzleException
     */
    public function storeOrUpdateContractor(string $code, bool $active = true): object
    {
        $this->setEndPoint('contractors');
        $this->addBodyParameter('code', $code);
        $this->addBodyParameter('active', $active);

        return PricesContractorsResponse::cast($this->post()->json());
    }
}
