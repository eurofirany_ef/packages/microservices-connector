# Docs for PHP 8 Version

* [Docs for PHP 7.3 Version](https://bitbucket.org/eurofiranki/microservices-connector/src/v7.3/README.md)

## Requirements

* Laravel ^8.0 / Lumen ^8.0
* PHP ^8.0
* ext-json

## Install

First install package with composer:

    composer require eurofirany/microservices-connector

Migrate tables for queue

    php artisan migrate

Next public config file to your app config folder:

    php artisan vendor:publish --tag=http_config

This generates **http.php** file in **config** folder

    'prices' => [
        'client' => env('PRICES_WS_CLIENT', ''),
        'token' => env('PRICES_WS_TOKEN', ''),
        'url' => env('PRICES_WS_URL', '')
    ],
    'stock' => [
        'client' => env('STOCK_WS_CLIENT', ''),
        'token' => env('STOCK_WS_TOKEN', ''),
        'url' => env('STOCK_WS_URL', '')
    ],
        'oracle' => [
        'client' => env('ORACLE_WS_CLIENT', ''),
        'token' => env('ORACLE_WS_TOKEN', ''),
        'url' => env('ORACLE_WS_URL', '')
    ],

    /** Queue Settings */
    'queue' => [
        'enabled' => false, // Is queue for connector enabled
        'max_tries' => 3, // Max tries for failed job
        'max_running_jobs' => 5, // Max running jobs at the same time
        'max_wait_seconds' => 300, // Wait seconds when there are too many jobs
        'sleep_seconds' => 10 // Seconds to wait between attempts
    ]

## Lumen Framework

Put this lines in **bootstrap/app.php** file:

    $app->register(\Eurofirany\ConnectorsQueue\Providers\ConnectorsQueueServiceProvider::class);
    $app->register(\Eurofirany\Microservices\Providers\MicroservicesConnectorServiceProvider::class);

    $app->configure('http');

For publish vendor config install **laravelista/lumen-vendor-publish** package 
or just create **http.php** file in **config** folder.